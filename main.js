window.onload = function () {
    new Vue({
    el: "#app",
    components: {
        Multiselect: window.VueMultiselect.default
    },
    data: {
        value: "",
        randomName: "",
        randomAlt: "",
        linkToShop: "https://www.amazon.de/s?k=",
        options: [
        { name: "Butter", cat: "Fett", alt: ["Leinsamenöl"] },
        { name: "Bratwurst", cat: "Fleisch", alt: ["Wild", "Pute", "Hähnchen", "Ente (ohne Haut)", "Kalbfleisch"] },
        { name: "Mortadella", cat: "Wurst", alt: ["Gekochter/ Geräucherter Schinken", "Lachsschinken", "Putenbrust"]  },
        { name: "Ente", cat: "Fleisch", alt: ["Wild", "Pute", "Hähnchen", "Ente (ohne Haut)", "Kalbfleisch"]  },
        { name: "Fischstäbchen", cat: "Fisch", alt: ["Gedünsteter/ Geräucherter Fisch", "Seelachs", "Kabeljau", "Schellfisch"]  },
        { name: "Gans", cat: "Fleisch", alt: ["Wild", "Pute", "Hähnchen", "Ente (ohne Haut)", "Kalbfleisch"]  },
        { name: "Hering", cat: "Fisch", alt: ["Gedünsteter/ Geräucherter Fisch", "Seelachs", "Kabeljau", "Schellfisch"]  },
        { name: "Lyoner", cat: "Wurst", alt: ["Gekochter/ Geräucherter Schinken", "Lachsschinken", "Putenbrust"]  },
        { name: "Makrele", cat: "Fisch", alt: ["Gedünsteter/ Geräucherter Fisch", "Seelachs", "Kabeljau", "Schellfisch"]  },
        { name: "Salami", cat: "Wurst", alt: ["Gekochter/ Geräucherter Schinken", "Lachsschinken", "Putenbrust"]  },
        { name: "Schweinebauch", cat: "Fleisch", alt: ["Wild", "Pute", "Hähnchen", "Ente (ohne Haut)", "Kalbfleisch"] },
        { name: "Speck", cat: "Wurst", alt: ["Gekochter/ Geräucherter Schinken", "Lachsschinken", "Putenbrust"]  },
        { name: "Reis (weiß)", cat: "Carbs", alt: ["Naturreis", "Quinoa", "Blumenkohl (geraspelt)", "Hirse"]  },
        { name: "Weizennudeln, z.B. Spaghetti", cat: "Carbs", alt: ["Zucchini- oder Kürbisnudeln (Zubereitung)", "Vollkornnudeln"]  },
        { name: "Bratkartoffeln", cat: "Carbs", alt: ["Pellkartoffeln", "Ofenkartoffeln", "Folienkartoffeln"]  },
        { name: "Kroketten", cat: "Carbs", alt: ["Pellkartoffeln", "Ofenkartoffeln", "Folienkartoffeln"]  },
        { name: "Pommes", cat: "Carbs", alt: ["Pellkartoffeln", "Ofenkartoffeln", "Folienkartoffeln"]  },
        { name: "Übliche Panade (Weizenbasis)", cat: "Carbs", alt: ["Sesam-Panade"]  },
        { name: "Croissant", cat: "Carbs", alt: ["Vollkorngebäck", "Dinkelgebäck", "Vollkorntoast"]  },
        { name: "Weißes Gebäck", cat: "Carbs", alt: ["Vollkorngebäck", "Dinkelgebäck", "Vollkorntoast"]  },
        { name: "Weißes Toast", cat: "Carbs", alt: ["Vollkorngebäck", "Dinkelgebäck", "Vollkorntoast"]  },
        { name: "Mayonnaise", cat: "Fette", alt: ["Öl", "Essig", "Senf", "Soja", "Salz"]  },
        { name: "Light Sauce", cat: "Fette", alt: ["Öl", "Essig", "Senf", "Soja", "Salz"]  },
        { name: "Fertige (Salat-)Sauce", cat: "Fette", alt: ["Öl", "Essig", "Senf", "Soja", "Salz"]  },
        { name: "Fruchtsaft", cat: "Fette", alt: ["Wasser mit Beeren", "Zitronenwasser", "Tee", "schwarzen Kaffee"]  },
        { name: "Limonade", cat: "Getränke", alt: ["Wasser mit Beeren", "Zitronenwasser", "Tee", "schwarzen Kaffee"]  },
        { name: "Bier", cat: "Getränke", alt: ["Radler", "Alster", "Weinschorle"]  },
        { name: "Erdnussbutter", cat: "Fette", alt: ["Cashewmus", "Mandelmus"]  },
        { name: "Nussnougat-Aufstrich", cat: "Süssigkeit", alt: ["100% Marmelade"]  },
        { name: "Flips", cat: "Süssigkeit", alt: ["Gemüsesnacks", "Gemüsesticks (Geschnittene Möhren/Paprika)"]  },
        { name: "Salzstangen", cat: "Süssigkeit", alt: ["Gemüsesnacks", "Gemüsesticks (Geschnittene Möhren/Paprika)"]  },
        { name: "Chips", cat: "Süssigkeit", alt: ["Gemüsesnacks", "Gemüsesticks (Geschnittene Möhren/Paprika)"]  },
        { name: "Sahneeis", cat: "Süssigkeit", alt: ["Frozen Yogurt", "Wassereis", "Sorbet"]  },
        { name: "Milchprodukte (Vollfett)", cat: "Milch", alt: ["Milchprodukte (fettarm)"]  },
        { name: "Crème fraîche", cat: "Milch", alt: ["Saure Sahn"]  },
        { name: "Mascarpone", cat: "Milch", alt: ["Körniger Frischkäse"]  },
        ]

        /* https://upfit.de/coach/ungesunde-lebensmittel-gesund-ersetzen/ 

https://www.amazon.de/s?k=

*/
    },
    computed:{
        sortedOptions: function() {
            return this.options.sort((t1,t2) => t1.name < t2.name ? -1 : 1);
        },
    },
    methods: {
        setFocus: function(){
            console.log(this.$refs);
            this.$refs.focused.$el.focus();
        },
        customLabel(option) {
            return `${option.name}`;
        },
        hideKeyboard(){
            
        }
    },
    created(){
        const idx = Math.floor(Math.random() * this.options.length);
        const idy = Math.floor(Math.random() * this.options[idx].alt.length);
        this.randomName = this.options[idx].name;
        this.randomAlt = this.options[idx].alt[idy];
    },
    mounted(){
        this.setFocus();
    }
    });
}